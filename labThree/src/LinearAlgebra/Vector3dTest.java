//Jose Carlos Betancourt Saiz de la Mora #1935788
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTest {

	@Test
	void testGets() {
		Vector3d vec1 = new Vector3d(1,2,3);
		assertEquals(1, vec1.getX());
		assertEquals(2, vec1.getY());
		assertEquals(3, vec1.getZ());
	}

	@Test
	void testMagnitude() {
		Vector3d vec1 = new Vector3d(1,2,3);
		assertEquals(3.7416573867739413, vec1.magnitude());
	} 
	
	@Test
	void testDotProd() {
		Vector3d vec1 = new Vector3d(1,2,3);
		Vector3d vec2 = new Vector3d(4,5,6);
		assertEquals(32, vec1.dotProduct(vec2));
		
	}
	
	@Test
	void testAdd() {
		Vector3d vec1 = new Vector3d(1,2,3);
		Vector3d vec2 = new Vector3d(4,5,6);

		Vector3d vec3 = vec1.add(vec2);

		assertEquals(5, vec3.getX());
		assertEquals(7, vec3.getY());
		assertEquals(9, vec3.getZ());
		
	}	
}
