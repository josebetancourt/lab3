//Jose Carlos Betancourt Saiz de la Mora #1935788
package LinearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	Vector3d(double x, double y, double z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public double getZ() {
		return this.z;
	}
	
	public double magnitude() {
		double magn = Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) +Math.pow(this.z, 2));
		return magn;
	}
	
	public double dotProduct(Vector3d vec1) {
		double dotProd = this.x * vec1.x + this.y * vec1.y + this.z * vec1.z;
		return dotProd;
	}
	
	public Vector3d add(Vector3d vec1) {
		Vector3d addVer = new Vector3d(this.x + vec1.x, this.y + vec1.y, this.z + vec1.z);
		return addVer;
	}
	
	public static void main(String[] args) {
		Vector3d vec1 = new Vector3d(1,2,3);
		Vector3d vec2 = new Vector3d(4,5,6);
		
		System.out.println(vec1.magnitude());
		System.out.println(vec1.dotProduct(vec2));
		
		Vector3d vec3 = vec1.add(vec2);
		double x = vec3.getX();
		double y = vec3.getY();
		double z = vec3.getZ();
		System.out.println(x + ", " + y + ", " + z);
	}
	
}
